const { app, BrowserWindow, globalShortcut, ipcMain } = require('electron');

let mainWindow;
let cssContent = '';

app.on('ready', () => {
  mainWindow = new BrowserWindow({
    width: 1200,
    height: 800,
  });

  // Load Krunker initially
  mainWindow.loadURL('https://krunker.io/');
});

app.on('will-quit', () => {
  // Unregister the global shortcut when quitting the app
  globalShortcut.unregisterAll();
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
